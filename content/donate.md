---
title: Donate
description: Donate to support continued work
keywords: donate
---

Thanks for reaching this page. If you donate through any of these options, you will directly be supporting [Akshay S Dinesh](https://asd.learnlearn.in/about/) in all the things that he is doing.

## Money ##

There is no minimum value for donation. If you are looking for a suggestion, ₹2000 would be a good figure to support an hour of my work. In dollars that would be about $30.

### Indians ###

You can transfer via UPI to [asdofindia@ybl](upi://pay?pa=asdofindia%40ybl&pn=Akshay)

### Outsiders ###

You can transfer via Paypal to [paypal.me/asdofindia](https://paypal.me/asdofindia)
