---
title: India
description: What is India?
keywords:
  - politics
---

India is a democratic country where every citizen has fundamental human rights.

## History past independence

* [Congress with respect to Rae Bareli and Amethi](https://www.thehindu.com/elections/lok-sabha/amethi-and-rae-bareli-twin-power-centres/article68140375.ece)
* [Congress, Ambedkarism](https://www.thenewsminute.com/long-form/mallikarjun-kharges-ism-an-ambedkarite-manifesto-for-the-modi-years)
