---
title: Public Health
description: To care for the health of an entire species
keywords: public health, community medicine, social medicine, preventive medicine
---
What if we kept treating people without worrying about who's contracting the disease, or why?

## DCP

Disease Control Priorities is a multi organization project to figure out which interventions lead to the most impact on health in low and middle income countries and elsewhere. Read [At Last! Universal Health Coverage That Prioritizes Health Impact: The Latest Edition of Disease Control Priorities (DCP3)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6024628/)

## Innovation

[Public Health Innovation Model](https://www.frontiersin.org/articles/10.3389/fpubh.2017.00192/full) is a model that incorporates design thinking from private sector into public health agencies.

## Grant opportunities

* [Grand Challenges](http://www.grandchallenges.ca/) by Canada has many opportunities