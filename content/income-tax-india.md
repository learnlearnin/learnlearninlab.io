---
title: Income Tax (India)
description: What is Income Tax in India and how to pay it?
keywords: income tax, it, india, finance
---

Income tax is really simple. You take all your incomes (not just salary, mind you, even some interests and other stuff), subtract all deductions and get the taxable income. Now see what block that taxable income falls under for your age and other demographics and pay that much tax.

So, the real knowledge you need to pay income tax correctly and even save some money is to know which incomes are taxable and which expenses are tax deductible. The problem is, sometimes, according to who you are, these categories are different. Also, over the years these keep changing.

Now you should wonder what determines these. Well, there is the [income tax act](https://www.incometaxindia.gov.in/pages/acts/income-tax-act.aspx) which has around 300 sections. (Click Chapter Wise and it is much easier to follow) You should probably read through and figure out what is what of tax. But tl;dr? This page.

I am going to extract relevant text out of the scores of sections of IT Act so you can go read it if it sounds important.

## Income Tax Act 1961-2018

### Section 1
"it shall come into force on the 1st day of April, 1962."

### Section 2
Definitions of terms. Defines things like agricultural income, assessment year, business, capital asset, charitable purpose, company, dividend, income, long-term capital asset, non-resident, 

* "assessment year" means the period of twelve months commencing on the 1st day of April every year ;
* "business" includes any trade, commerce or manufacture or any adventure or concern in the nature of trade, commerce or manufacture;
* "income" includes—

    * (i)  profits and gains ;
    * (ii)  dividend ;
    * (iii)  the value of any perquisite or profit in lieu of salary taxable under clauses (2) and (3) of section 17 ;
    * (iiia)  any special allowance or benefit, other than perquisite included under sub-clause (iii), specifically granted to the assessee to meet expenses wholly, necessarily and exclusively for the performance of the duties of an office or employment of profit ;
    * (iiib)  any allowance granted to the assessee either to meet his personal expenses at the place where the duties of his office or employment of profit are ordinarily performed by him or at a place where he ordinarily resides or to compensate him for the increased cost of living ;
    * (iv)  the value of any benefit or perquisite, whether convertible into money or not, obtained from a company either by a director or by a person who has a substantial interest in the company, or by a relative of the director or such person, and any sum paid by any such company in respect of any obligation which, but for such payment, would have been payable by the director or other person aforesaid ;
    * (ix) any winnings from lotteries, crossword puzzles, races including horse races, card games and other games of any sort or from gambling or betting of any form or nature whatsoever.
    
    Further there are  clauses which include section 28, 41, 59, 45, 56.

    28, 41, 45, 56, 59 in sorted order

### Section 3
Previous year. Just means financial year before the assessment year.

### Section 10
Begins incomes not included in income.

The clause 13A and footnote 43 tells you how to exempt house rent.
The clause 14 and rule 2BB linked in footnote tells you how to calculate things like travel allowance

### Section 28
Makes businesses and professions income taxable

### Section 41
Regarding loss in business which becomes profit

### Section 45
Regarding profit from capital transfer

### Section 56
**Any other income not mentioned elsewhere** (this is the catch all statement)