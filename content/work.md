---
title: Work
description: Things about work
keywords: non-profit, work
---

### Good reads

* [Radical Candor](https://www.radicalcandor.com) on caring personally while challenging directly

### Burn-out

* [Myths that perpetuate burn-out](https://ssir.org/articles/entry/five_myths_that_perpetuate_burnout_across_nonprofits) - pay people more, make life easier for employees, communicate internally as much as externally, colleagues are not family, leaders ought to set examples of work-life balance


### Disillusionment

* [Wheel of disillusionment](https://nonprofitaf.com/2018/11/the-wheel-of-disillusionment-what-it-is-and-how-it-destroys-relationships/) - talks about triangulation which is when people are talking to others instead of each other when there's a problem. Links to radical candor above. Difficult conversations.
* [How to overcome cynicism](https://scottberkun.com/2014/how-to-overcome-cynicism/)
