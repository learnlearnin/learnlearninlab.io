---
title: Otorhinolaryngology (Ear, Nose, Throat, Head, Neck)
description: ENT deals with things from your neck up and is the branch of medicine in which common cold is treated under
keywords: ent, laryngology, otology, rhinology
---

## Vertigo ##

Vertigo is a common complaint in ENT. [It is a symptom, not a diagnosis](https://acountrydoctorwrites.blog/2019/08/29/vertigo-is-a-symptom-not-a-diagnosis-and-its-sometimes-caused-by-loose-rocks-inside-your-head/). Do read about [otoliths](http://www.dizziness-and-balance.com/disorders/bppv/otoliths.html)

## Links ##
### Clinical ###

 * [All instruments](http://entinstruments.blogspot.com)

