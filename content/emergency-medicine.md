---
title: Emergency Medicine
description: Department of Emergency Medicine
keywords: emergency medicine
---
When it comes to life and death situations, emergency medicine enters the scene. As soon as there is certainty as to what happens next, emergency medicine bows out.

The things that come under emergency medicine include, not limited to
* [Trauma](../trauma/) - what to prioritize, what to be careful of
