---
title: Porn Unban
description: A firefox addon that redirects blocked porn sites to mirrors
keywords: firefox, extension
---

[Source Code](https://gitlab.com/asdofindia/porn-unban/)

[Download v0.0.4](https://github.com/asdofindia/porn-unban/releases/download/v0.0.4/porn_unban-0.0.4-an+fx.xpi) - adds streamable.com support

[Download v0.0.3](https://github.com/asdofindia/porn-unban/releases/download/v0.0.3/porn_unban-0.0.3-an+fx.xpi)
