---
title: "Beat Clock Privacy Policy"
---

Beat Clock is a widget that does not collect any data.

Hence, there is no privacy issue caused by usage of Beat Clock.
