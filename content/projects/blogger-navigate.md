---
title: Blogger Navigate
description: A firefox addon that adds a next post link in URL bar for blogger blog posts
keywords: firefox, extension
---

[Source Code](https://github.com/asdofindia/blogger-navigate-extension/)

[Download v0.0.1](https://github.com/asdofindia/blogger-navigate-extension/releases/download/v0.0.1/blogger_navigate-0.0.1-an+fx.xpi)
