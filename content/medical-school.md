---
title: Medical School
description: Getting through medical school
keywords: medical school
---

The medical school is an unavoidable checkpoint in the path to becoming a doctor. The internship, especially, is gruelling for most people.

Here are a few articles on the attitude that can help you through:
* [The Important Thing-Intern in Surgery](https://healthylifehappylife.in/the-important-thing-intern-in-surgery/)
* [How Working On Christmas Became A Privilege For 2 Young Doctors](https://www.npr.org/sections/health-shots/2018/12/25/678034690/how-working-on-christmas-became-a-privilege-for-2-young-doctors)
