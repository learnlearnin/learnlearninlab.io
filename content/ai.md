---
title: Artificial Intelligence
description: To emulate intelligence is easy. But to be intelligent is not.
keywords: machine learning, artificial intelligence
---

AI is to computers what common sense is to man.

## Ethics ##

To understand why the question of ethics arises in AI, one has to understand what [pitfalls exist in AI](https://www.harvardmagazine.com/2019/01/artificial-intelligence-limitations). The limitations of AI eventually leads to unintended harms. That is where the questions on ethics come from.

## LLMs ##

### Tools

* [LMSYS](https://lmsys.org/) - Large Model Systems Organization has various tools like [Arena](https://arena.lmsys.org/) to benchmark LLMs

### Hacking

* https://rentry.org/GPT2 - example of how to fingerprint an LLM
