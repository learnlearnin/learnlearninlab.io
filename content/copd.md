---
title: COPD
description: Chronic Obstructive Pulmonary Disease
keywords: copd, respiratory, lungs
---
[The Global initiative for chronic Obstructive Lung Disease (GOLD)](http://www.goldcopd.org/) brings out the recommendations for COPD.

[Read 2015 guidelines](http://www.goldcopd.org/uploads/users/files/GOLD_Report_2015_Feb18.pdf) (PDF, 2.1MB)

> Chronic Obstructive Pulmonary Disease (COPD), a common preventable and treatable disease, is characterized by persistent airflow limitation that is usually progressive and associated with an enhanced chronic inflammatory response in the airways and the lung to noxious particles or gases. Exacerbations and comorbidities contribute to the overall severity in individual patients. <footer>GOLD Report 2015</footer>

## Risk Factors ##
* **Smoking** is the single largest risk factor for COPD. Everything else fades in comparison.
* Airway hyperresponsiveness predicts decrease in pulmonary function.
* Occupational exposure (espcially coal mine workers), ambient air pollution (urban, biomass) fade in comparison to smoking.
* Respiratory infections are yet to be proved as an association.
* Genetics - alpha 1 antitrypsin deficiency

## Papers ##

* [The Pathological Changes in Chronic Bronchitis and Emphysema - Lynne Reid(1958)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2501035/pdf/postmedj00494-0026.pdf)
  * Review [histology of bronchial tree here](https://training.seer.cancer.gov/anatomy/respiratory/passages/bronchi.html)
