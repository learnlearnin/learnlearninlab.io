---
title: Spirituality
description: Questions about deeper meanings of life
keywords: spirituality, religion
---

As a religious practice, I hate spirituality.

But I have forayed into spirituality when I was young and questioning everything. I have documented most of my thoughts on my blog. New ideas in this topic still interest me, but I have felt that most often it's not new ideas that I hear, but new people saying the same old things I have discovered.

Vivekananda talks about the idea of [God as super consciousness](http://blog.learnlearn.in/2009/08/god-as-cosmic-consciousness.html). Reading his works had some [effect on my ambitions](http://blog.learnlearn.in/2009/03/worst-thing-for-ambitious-young-man-is.html).

I have also written about [my views on God](http://blog.learnlearn.in/2008/09/what-i-believe-about-god.html).

My spirituality, on the other hand leans towards astronomy. It's the same kind of philosophy that Carl Sagan had when he saw the [pale blue dot](http://blog.learnlearn.in/2009/10/pale-blue-dot.html) that earth was. My latest thoughts on this topic is that being able to [imagine nothing](http://blog.learnlearn.in/2009/10/imagining-nothing.html) is the ultimate answer to everything.
