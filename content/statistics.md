---
title: Statistics
description: The math of life
keywords: statistics, biostatistics
---

Some links for stuff I want (you) to read:
* [Revisiting a 90-year-old debate: the advantages of the mean deviation](http://www.leeds.ac.uk/educol/documents/00003759.htm) - Why do you use standard deviation? Why can't you use mean deviation?
* [Robust Statistics](https://en.wikipedia.org/wiki/Robust_statistics)
* [What is an Intracluster Correlation Coefficient](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1466680/)

### Regression Analysis ###

There is nothing great about regression analysis. It is just figuring out how one variable changes another variable. [Why is it named so then?](http://blog.minitab.com/blog/statistics-and-quality-data-analysis/so-why-is-it-called-regression-anyway). When you read that you also get linked to [10 statistical terms designed to confuse non-statisticians](http://blog.minitab.com/blog/understanding-statistics/10-statistical-terms-designed-to-confuse-non-statisticians).


