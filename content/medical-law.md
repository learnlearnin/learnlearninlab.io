---
title: Medical Law
description: Medical Law in India
keywords: health, medical, healthcare, law
---

## Important Laws ##

### Consumer Protection ###

* [Indian Medical Association vs VP Shantha, 1995](https://indiankanoon.org/doc/723973/) - where the Supreme Court settles the question of what kind of medical practice comes under the definition of "service" as defined in the consumer protection act.
* [Kusum Sharma vs Batra Hospital, 2010](https://indiankanoon.org/doc/29738758/) - where Supreme Court lays principles to be kept in view when deciding whether medical professional is guilty of medical negligence.
