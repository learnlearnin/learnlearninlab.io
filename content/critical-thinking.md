---
title: "Critical Thinking"
---

[Small guide](https://blog.learnlearn.in/2011/03/small-guide-to-critical-thinking.html)

Whatever you read, whatever you hear, whatever you think, ask yourself:

Could the opposite be true?


## Cognitive Biases

Critical thinking is mostly helped by figuring out our own cognitive biases and accounting for those in our thinking.

### Confirmation bias

When we have made up our mind about something we tend to see factors/signs in favor of our already made conclusion only. This cherry-picking of what we want to see is going to make us believe more strongly in what we are believing. We could become more and more wrong. 

How do we protect ourselves from confirmation bias? 

* Become aware of it.
* Imagine the possibility of us being wrong.
* Think about alternate possibilities.
* Look for evidence supporting any of the alternatives.


Confirmation bias is arguably the most important cognitive bias to be aware of. Once you figure out a way to tell yourself you could be wrong, you can figure out numerous ways in which you could be wrong (corresponding to numerous biases). Stereotypes that lead to discrimination is also arising from these kind of biases.

### Other biases

[Read about more biases](https://www.verywellmind.com/cognitive-biases-distort-thinking-2794763). You should know at least these.

* hindsight bias
* post-facto rationalization
* anchoring bias
* false consensus effect
* halo effect / first impression effect
* correlation is not causation


There are many many more of these traps. The more we are aware of, the more we can protect ourselves from it.

Halo effect, for example. There are many hundred times where I have looked at fair skinned, tall people and instantly assumed they're smart. This leads to discrimination and disappointment.

## Biology

Biases are rooted in our brain biology. It is quicker and less energy intensive to think through biased ways. This is perhaps arising from us being animals. ([Or maybe not](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9364952/))

## Change

Mere awareness of biases is not enough. We will have to do the difficult, embarrassing, vulnerable thing called changing ourselves. We will have to change our beliefs.

[Outsmart Your Own Biases](https://hbr.org/2015/05/outsmart-your-own-biases)