---
title: Human Immunodeficiency Virus
description: What is HIV?
keywords: hiv, aids
---
* What is the natural life cycle of HIV in human?

It is interesting to read about the genetics/evolution of HIV.

[**HIV: the ultimate evolver** page on Berkeley's Evolution Library](https://evolution.berkeley.edu/evolibrary/article/medicine_04) tells the incredible story of one of the fastest evolving entities known. You will also meet SIV, [FIV](https://mbbshacker.blogspot.com/2017/08/do-cats-get-hiv.html), CCR5 mutation that helps against some strains of HIV, and drug resistance on this page.

## HIV Tropism ##

So, why does the CCR5-Δ32 mutation confer protection from only some strains? To answer that you will have to learn about HIV tropism.

[HIV-1 receptors and cell tropism](https://academic.oup.com/bmb/article/58/1/43/337216) is a free article from British Medical Bulletin which you can read if you know something about fusion in lifecycle of HIV. Otherwise, come back here once you finish reading about fusion. This article talks about how gp120 connects with CD4 and then how the co-receptor CCR5 is important for entry, but also how even in people with CCR5-Δ32 mutation who didn't have CCR5 HIV was found which led to the knowledge that CXCR4 can also be a co-receptor. It also talks about so many other co-receptors (which aren't really active in vivo but helps in binding when the concentration of CD4 is low)

[HIV-1 Coreceptor Use: A Molecular Window into Viral Tropism](https://www.hiv.lanl.gov/content/sequence/HIV/REVIEWS/doms.html) (a review article by the HIV databases project hosted by Los Alamos National Laboratory)

[Understanding HIV Tropism](https://www.prn.org/index.php/management/article/hiv_tropism_1002) article on Physician's Research Network has nice photos that explain all of these facts and more.

So if you just wanted to know more about CCR5 mutation, read this [simple worded answer to "Are any people genetically predisposed to being immune to HIV?"](https://genetics.thetech.org/ask/ask336). They even think [bubonic plague lead to the selection of this mutation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC299980/)

At this point you should have been exposed to R5/X4 classification, NSI/SI classification, M-tropic/T-tropic/TCLA classification, slow-low/rapid-high classification.

These are classifications of HIV based on just the tropism. But the more widely classification is based on genotype.

## HIV Classification ##

What kind of virus is HIV? Lentivirus? What is that? All answered in [this page on HIV classification](http://bioweb.uwlax.edu/bio203/s2008/kinsley_kayl/classification.htm) by a student. Retrovirus is an important point to note. It goes against the dogma of genetics.

[HIV and SIV Nomenclature](https://www.hiv.lanl.gov/content/sequence/HelpDocs/subtypes-more.html) from HIV databases again documents very neatly (and updated-ly) all the types of HIV. It introduces you to HIV-1, HIV-2; subtypes, sub-subtypes, CRFs, etc. As linked on the page, this is based on the [consensus formed in 1999](https://www.hiv.lanl.gov/content/sequence/HIV/REVIEWS/nomenclature/Nomen.html)

Here is another [page on the HIV-1 subtypes](https://www.aidsmap.com/HIV-1-subtypes/page/1322996/) and on this page you will find a citation to [High replication fitness and transmission efficiency of HIV-1 subtype C from India: Implications for subtype C predominance](https://www.sciencedirect.com/science/article/pii/S0042682208008477) which helps you remember that subtype C is common in India.

## Readings ##

* [Top HIV/AIDS Research papers](https://academic.oup.com/ofid/article/6/8/ofz348/5546064)



## Resistance ##
One important factor to remember is that most of the resistance studies have been done on HIV-1 subtype B and does not really help other regions.

[Watch TED Talk: Edsel Salvaña: The dangerous evolution of HIV](https://www.ted.com/talks/edsel_salvana_the_dangerous_evolution_of_hiv) which describes this issue. India predominantly has HIV subtype C.

## Replication Cycle ##
[Neat diagram about HIV Replication Cycle on Viral Zone](https://viralzone.expasy.org/5096)

## Acute HIV ##

What is acute HIV? Does it present with any symptoms?

Read up [Fiebig staging](https://www.projectinform.org/glossary/fiebig-stages/)

## Chronic Care ##

Watch [this video by Dr Gordon Liu](https://www.youtube.com/watch?v=Mz4n2unyj04) to know about chronic inflammation, cancer screening, [depression](../mental-health/#depression) and other aspects of primary care in HIV.
