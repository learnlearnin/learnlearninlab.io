---
title: Medical Procedures
description: Various clinical outpatient/inpatient procedures
keywords: procedures, medical
---

## Central Venous Catheter ##

* [Acute hypotension following CVC placement](https://knowledgeplus.nejm.org/blog/distinguishing-between-a-hemothorax-and-a-pneumothorax/)
