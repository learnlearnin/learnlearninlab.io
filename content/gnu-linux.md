---
title: GNU/Linux
description: GNU/Linux is the sweetness of a fruit juice.
keywords: GNU, linux, GNU-Linux, os
---
Computers are what they are because of what GNU/Linux has done to the world. My favorite GNU/Linux flavor is [arch](../archlinux/).

Note: It is GNU/Linux, not [Linux](../linux/). Read Why [GNU/Linux](https://www.gnu.org/gnu/why-gnu-linux.html).

## Must read ##

* [Things You Didn't Know About GNU Readline](https://twobithistory.org/2019/08/22/readline.html)
* [History and effective use of Vim](https://begriffs.com/posts/2019-07-19-history-use-vim.html)
