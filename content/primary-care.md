---
title: Primary Care
description: Primary care is the first and top-most level of care
keywords: primary medical care, primary care
---

## Charisma ##

A primary care physician needs to [be the doctor that their patient needs them to be](https://acountrydoctorwrites.blog/2018/10/11/be-the-doctor-each-patient-needs/). They should [build charisma](https://acountrydoctorwrites.blog/2019/08/19/cultivating-charisma-in-the-clinical-encounter-and-emulating-marcus-welby-m-d/)
