---
title: Emotional Health
description: What's emotional health, how to increase
keywords: mental health
---

* [Emotional Literacy for Better Mental Health | Shahana Alibhai | TEDxAbbotsford](https://www.youtube.com/watch?v=eu9oSkCe1e0)
  * Questions to ask
    * Can you label and identify the exact thoughts?
    * How have you coped when things have not gone your way?
    * What do you want to be later? Tell me about your dreams.
