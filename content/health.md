---
title: Health
description: Health is the currency of life.
keywords: health, medicine, healthcare
---

When wealth is lost, everything is lost. When health is lost, you die.

## Health For All

* [PHA5 discussion paper](https://phmovement.org/sites/default/files/2024-03/EN%20DiscussionPaper4PHA5.pdf)

