---
title: Medical Home Regulations
description: What are the legal requirements in starting one's own medical home in Karnataka, India
keywords: medical, clinic, regulations, karnataka
---

This page is written for collating information regarding the legal/regulatory requirements for setting up a medical home (clinic + diagnostics + pharmacy + treatment)  in Bangalore, Karnataka, India in 2019. There is a school of thought that the law never changes, only our understanding of the law changes. In fact, our understanding of law changes so quickly and revolutionarily that what is written on Monday maybe outdated on Tuesday. Also to be expected are a lot of omissions and errors.

## Background ##

Health is a subject in the [State List](https://en.wikipedia.org/wiki/State_List) in India. What that means is that only states have the power to make laws in health. (That is why, if you noticed, Ayushman Bharath had to be accepted by states before it could be rolled out). But a medical home is not just about "health". There are a hundred other concerns. Environment, trade, ethics, whatever you can think of. Therefore there are laws at state level and national level that one has to be aware of. Even the local governing body (especially in metros like Bangalore) would have made regulations. We will try to cover them one by one.

## Licenses ##


### Trade license ###

The [shops and establishments act](http://dpal.kar.nic.in/8%20of%201962%20(E).pdf) makes it mandatory to get a trade license for any establishment. ("offices of legal practitioners and medical practitioners in which not more than three persons are employed" are exempted).

Trade license can be applied for [online in Bangalore](http://bbmp.gov.in/en/web/guest/tradelicensemain). Read more about [trade license here](https://www.lawfarm.in/blogs/obtaining-trade-license-under-shop-and-establishment-act). In 2017 private hospitals had put pressure on CM and made it [not mandatory](http://www.newindianexpress.com/states/karnataka/2017/sep/24/medical-facilities-dont-need-trade-licence-anymore-1661899.html), but it is [still mandatory](http://www.newindianexpress.com/cities/bengaluru/2018/oct/15/trade-licence-still-must-for-hospitals-says-bengaluru-urban-development-department-1885629.html)

The required documents for trade license includes:

* Consent from owner
* No-objection certificate from immediate neighbours (if in residential zone)
* Layout plan of trade
* Solid Waste Management cess reciept

License for power (or generator use) is also obtained in the same application.

This license needs to be renewed.

### Labour license ###

?[Not applicable](https://labouronline.kar.nic.in/Home.aspx)

### Labour Laws ###

[This pdf](http://www.ncib.in/pdf/ncib_pdf/Labour%20Act.pdf) gives an overview of the many labour related laws that exist in India.

### KPME ###

**Karnataka Private Medical Establishments** Act which was amended recently leading to lots of debates regulates medical establishments. There is an [online portal](https://karhfw.gov.in/kpme/SignInPage.aspx).

The rules [are online](https://www.karnataka.gov.in/hfw/kannada/Documents/Draft_Rules-KPME%20Act%2027.03.2018.pdf) especially Form A. There are many requirements some of which are:
* Floor plan
* Front view photograph
* Occupancy certificate
* Fire safety certificate
* Details of all staff including registration number
* Internal grievance redressal mechanism


#### Fire safety certificate ####

This probably refers to an NOC from fire department. There is an [online portal](http://agni2.mrc.gov.in/KARNATAKAFIREPORTAL/Home.aspx)

The [requirements](http://agni2.mrc.gov.in/KARNATAKAFIREPORTAL/App_Themes/Blue/Images/docs/Fire_Required%20Documents-2018%20April.pdf) for that seem to be:
* Site plan
* Ground floor plan
* Built up area statement
* Section
* Elevation
* Schematic drawing of firefighting system
* Schematic drawing of fire detecting system
* Typical floor plan indicating firefighting and preventing provisions including water tank

After online submission there will be inspection of the facility


### Karnataka State Pollution Control Board ###

KSPCB takes care of air, water, waste, etc. [Forms are available online](https://www.kspcb.gov.in/downloadableforms.html).

Which consents are required depends on the [category of industry](https://www.kspcb.gov.in/consentCategory.html). Red category includes health-care establishment (as defined in BMW rules). But white category includes non-bedded HCEs and HCEs less than 30 beds.

[Link to BMW rules, 2016](http://mpcb.gov.in/biomedical/pdf/BMW_Rules_2016.pdf) (from Maharashtra's website)
[Amendment in 2018](https://www.kspcb.gov.in/BMW-(A)Rules-2016.pdf)
[Amendment in 2019](https://www.kspcb.gov.in/BMW-(A)Rules-2016.pdf)

#### Biomedical waste ####

If one considers a "clinic", which should come under white category, there are is at least one form to be filled as per BMW rules.

That is: [Form No. II Under Bio Medical Waste (Management & Handling) Rules 2016 [Rule(10)]](https://www.kspcb.gov.in/BMW_FormII.pdf) - Application For Authorization or Renewal Of Authorization(Management & Handling) Rules 2016

The rule 10 also says "The authorisation shall be one  time  for non-bedded occupiersand  the  authorisation in such  cases shall be deemed to have been granted, if not objected by the prescribed authority within a period of ninety  days  from  the  date  of  receipt  of  duly  completed  application  along  with  such  necessary documents."

#### Water Cess ####

Monthly returns regarding water consumption have to be made and the form is available on this website.
[Rules](http://cpcb.nic.in/displaypdf.php?id=aG9tZS93YXRlci1wb2xsdXRpb24vR1NSLTM3OEUucGRm)
[Act](http://cpcb.nic.in/displaypdf.php?id=aG9tZS93YXRlci1wb2xsdXRpb24vRG9jMi5wZGY=)

### PCPNDT ###

Required only if scanning facility. [Here is the act book](http://www.pcpndt.karnataka.gov.in/PDFs/PCPNDT%20Act%20Book%20English.pdf)

### VAT ###

Possibly replaced by [GST](#gst)

### Service Tax ###

Possibly replaced by [GST](#gst)

### GST ###

We are now in GST regime. Everything is based on GST - credit, debit, blah blah. There are books written about GST.

[Services provided by health care services by a clinical establishment, an authorised medical practitioner or para-medics](http://ctax.kar.nic.in/latestupdates/602%20-%20IVA%20-%20FD%2048%20CSL%202017-12.pdf) may not come under GST.


### Professional Tax ###

On behalf of employees, professional tax may have to be paid. [Online](http://pt.kar.nic.in/)

### BBMP ###

You may have to make sure the property that the clinic operates out of is in a [commerical area](http://bbmp.gov.in/documents/10180/504904/Zoning_Regulations_RMP2015f.pdf/0a916060-b198-4903-b7cd-d18db7096ebd).

### Medicare Waste Management ###

### Certificate Of Incorporation ###

### Foreign Trade-for importer & exporter code no ###

### Registration Certificate of establishment ###

### Registration under company act ###

### Electricity installation certificate ###

### Atomic energy ###

### Drug sale license ###

### Narcotic Drugs and Psychotropic Substances ###

### Excise permit for spirit ###

### License for treating psychiatric patients ###

