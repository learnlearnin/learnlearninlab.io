---
title: Ideas
description: A lot of ideas
keywords: ideas
---

## Medical Research ##

* Trophoblast invades into the decidua of the mother and there's contact of maternal blood with it. Yet there's no immune reaction. Why?

## Patient education ##

* Can a live/working model of a lung be used such that when patients deposit their bidi/cigarette into it, the lung turns black proportionate to how much bidi/cigarette would turn in black death?
* Explore Virtual Reality as a means of patient education
