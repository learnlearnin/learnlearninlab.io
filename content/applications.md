---
title: My favorite (preferred/used) applications
keywords: apps, recommendations, linux, android
description: A collection of my favorite applications for each purposes.
---
Arch Wiki has a [huge list of applications](https://wiki.archlinux.org/index.php/List_of_applications) for most things.
This is probably a subset of that.

## Internet ##

### General ###

* [Firefox](../firefox/) - for web browsing
* Transmission - for p2p
* [FreshRSS](https://freshrss.github.io/FreshRSS/) - for RSS

### Communication ###

#### Email ####

* Thunderbird - Desktop
* [FairMail](https://email.faircode.eu/) - Android

#### Chat ####

* [HexChat](http://hexchat.github.io/) - for IRC
* [Convos](https://github.com/Nordaaker/convos/) - for cloud IRC
* [Element](https://element.io/) - for Matrix.org chat
* Telegram - for large groups

## Multimedia ##
* GIMP - for photo editing
* ImageMagick - for command line image manipulation (`convert something.png something.jpg`)
* Clementine - music player
* VLC - video player
* Audacity - audio editing
* Kdenlive - for simple video editing

## Utilities ##
* vim - you know, but at the same time I like Emacs and VS Code too.
* Intellij Idea for Java/Android projects
* [Redshift](http://jonls.dk/redshift/) - color adjustment

## Shell ##
* ZSH

## Documents ##
* Calibre - for epub and everything!

