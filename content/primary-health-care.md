---
title: "Primary Health Care"
description: "Primary care is the field of medicine that deals with care at the primary level. Primary health care is a much broader field that takes care of the entire health needs"
---

## Business Models ##

What are some of the business models in primary health care?

## Organizations ##



### Rural Health Collective ###

The rural health collective is a group of organisations involved in rural
health work:

1. Ashwini, Gudalur, Tamil Nadu http://ashwini.org/new/

2. Basic Health Services, Rajasthan https://bhs.org.in/ - high quality, low cost primary health care to last mile communities

3. Tribal Health Initiative, Sittilingi, Tamil Nadu
http://www.tribalhealth.org/

4. Primary Health Care Program, KCPatti, Tamil Nadu
http://kcpphc.org/home/about/

5. Swasthya Swaraj, Kalahandi, Odisha http://www.swasthyaswaraj.org/

6. Christian Hospital and Mitra, Bissam Cuttack, Odisha
http://chbmck.org/

