---
title: Cool Resources in Medicine
description: These are some third party content that helps one learn medicine
keywords: medicine, medical, resources
---

## General ##

* [NEJM alerts](https://www.nejm.org/nejm-alerts?query=TOC) - you get to see a lot of content from NEJM including interesting cases and challenges like spot diagnosis

## Ophthalmology ##

* [ophthobook](https://timroot.com/ophthobook/) - nice online ophthalmology textbook

## Pathology ##

* [Pathology Student](https://www.pathologystudent.com/) - an incredible blog to learn pathology. Make sure you subscribe to the [newsletter](https://www.pathologystudent.com/free-stuff/)

## Emergency Medicine ##

* [LIFE](https://oxlifeproject.org/) - Life-saving Instruction for Emergencies (there is an app)
