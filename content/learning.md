---
title: "Learning"
description: "What does it mean to learn?"
keywords: learning
---
What does it mean to learn?

Dr Shekar, my professor of Medicine used to say that there are a lot many levels of learning.

## Theory ##
[Bloom's Taxonomy](https://en.wikipedia.org/wiki/Bloom%27s_taxonomy) helps us talk about learning.

Domains of learning
1. Cognitive (knowing)
2. Affective (feeling)
3. Psychomotor (doing)

I'm interested in cognitive learning. It can be considered as having different levels.
* Knowledge
* Comprehension
* Application
* Analysis
* Evaluation
* Synthesis

As you go down the above list you realize that the effectiveness of learning increases. So, our learning should always be about preparing us to synthesize knowledge.

## Practice ##
When there's a moderate amount of information and there's only one day for an exam, when we are cramming, we improve knowledge.

But two-three days before, if we are reading, we would also be comprehending.

If after an exam we are asked to implement the knowledge in some practical project, we reach the application level. It is not necessary that we actually do a project, we can simulate a project in our mind.

These three levels are usually followed by students in the normal course of their life.

Analysis is breaking down the knowledge into constituent parts and identifying their relations to each other. This is done through analogies, stories, hypothetical "reasons", etc.

Evaluation is when we start thinking about the knowledge and its application to real life. About how effective that is in real life.

Synthesis is when we come up with alternative solutions to problems we discover during evaluation.

By its nature, the latter three takes some time while doing. And that is probably why students avoid these. Time management might help.

## Learning Pit ##

Learning is challenging for everyone. If there is no challenge, either there is no new learning or you aren't doing it correctly. The pit is that point in time when you are trying to learn but nothing is happening. Learning is all about going into the pit and then climbing back up. [Read more about it in James Nottingham's blog](https://sustainedsuccess.blogspot.com/2009/06/learning-pit.html)

## Exams ##
I like [open book exams](http://www.iiserpune.ac.in/~mohanan/educ/openbook.pdf). It is difficult for a teacher to create an open book question paper and that is probably why these kind of tests are rarely seen.

I like frequent examinations. Once a week is ideal.

## Specialization ##

[I love this illustrated guide to PhD](http://matt.might.net/articles/phd-school-in-pictures/) by the wonderful [Professor Matt Might](http://matt.might.net/articles/tenure/). What I like about it is not that doing PhD helps you make a dent in the circle of all human knowledge. It is that even if you do PhD you can make only a very small dent in the circle.

Sure that excites some people. But what really excites me is the prospect of being able to cover as much area as possible of the circle. I believe in [despecialization](https://bigthink.com/praxis/how-to-be-a-polymath).

It might make sense for at least some people to learn multiple things as this broad knowledge might generate crucial insights that can solve certain problems of the world. Besides, learning is fun and there is no reason one should restrict oneself to one or two particular topics.

## Tacit Knowledge ##

There are certain things that are too hard to put in words. For example, in medical school experienced doctors might make a spot diagnosis (based on many perceptible and imperceptible signs) and try to explain why they made the diagnosis - and learners will be as confused as they were before, just nodding "yes". The reason is that the former doctor would have relied on tacit knowledge in pattern matching over those imperceptible signs. These, this doctor will not realize they've used in reaching a diagnosis and in turn the learners won't hear about these either.

Read [this essay about why tacit knowledge is real](https://commoncog.com/blog/tacit-knowledge-is-a-real-thing/) and as many links as possible on that blog.

## Adult Learning ##

* https://en.wikipedia.org/wiki/Lifelong_learning
* https://en.wikipedia.org/wiki/Andragogy
* https://en.wikipedia.org/wiki/Adult_education
* [Methodology, Fake Learning, and Emotional Performativity](https://journals.sagepub.com/doi/10.1177/2096531120984786)
* 

## Outdoor Learning ##

* https://link.springer.com/chapter/10.1007/978-3-030-75980-3_15?fromPaywallRec=false
* https://link.springer.com/chapter/10.1007/978-94-6209-215-0_2?fromPaywallRec=false
* https://link.springer.com/content/pdf/10.1007/978-3-031-04108-2_1.pdf?pdf=inline%20link
* https://link.springer.com/chapter/10.1007/978-3-031-04108-2_1
* https://archive.org/details/in.ernet.dli.2015.155699/page/n593/mode/2up?view=theater

## Psychology ##

* https://en.m.wikipedia.org/wiki/Thomas_theorem#Definition_of_the_situation
* https://en.m.wikipedia.org/wiki/Superficial_charm
* https://en.m.wikipedia.org/wiki/Impression_management
* https://en.m.wikipedia.org/wiki/The_Presentation_of_Self_in_Everyday_Life

## Freire etc
* [Barefoot college](https://sci-hub.se/10.1162/itgg.2008.3.2.67)

