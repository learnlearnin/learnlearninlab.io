---
title: "mfc as thought-current"
---

### Discussions in the past

Randall [respectfully asked after a session on the origins of mfc as to when it was restricted to a thought-current](https://groups.google.com/g/mfccircle/c/nzuI_fLJeS0/m/3MEnOUTREQAJ):

> How and when did the evolution and 'fixed nature' from action and demonstration (as Anant put it in his where do we go from here letter and Ravi mentioned of the various community health projects-jamkhed,Mandwa etc) to becoming a 'Thought-current' happen. Some of the Why is certainly answered for in the debate between Anant and Imrana/Abhay but not so much the How and when . Also the name 'thought-current'. how did this come about? It is an interesting neologism and also in a way limits the MFC space to be more of debate and discussion as well as 'academic'
> How and Why was there an evolution into a more intellectually coded forum due to which the membership of younger medical,nursing students also dropped (did it happen because of the evolved origin or much later). Was there an unconscious evolution into an academic space rather than a practitioners space? Or is this a polarity I am creating from my vantage point and perspective based on my involvement in the last few years? - some of the papers in the bulletin do have a propensity for jargon and academic language that is at odds with the demystification objective in general

Rajeev also replied:

> Ashok Bhargava mentioned during the panel discussion that mfc was conceptualised as a thought current. I am not sure if mfc rightly fits into the bracket of thought current. Because mfc is a platform where discussions of various social issues directly and indirectly related to health are taking place, it can be perceived as a thought current. These rich discussions are also helping in some ways to take some action, so figuratively, I do not think thought current applies very well. mfc is a collection of individuals and organisations who are deeply involved in bringing about change. Besides, introspection and discussions are required for taking action on the work we are all engaged in. 
> I do not think the binary of thought current/action exists in reality, but everything lies in the spectrum of things and one can always identify themselves where they belong at that point in time. 

And Ashok Bhargava clarified that it was not a thought current at the start.

> I am sorry if my presentation gave the impression that MFC was started as a thought current. MFC was an offshoot of Tarun Shanti Sena which was an activity oriented group so thought without activity was out of question. MFC was part and parcel of Tarun Shanti Sena, therefore I gave an overview of Tarun Shanti Sena in my presentation.

Ritu also clarified that it was not just thought current:

> I am sorry if I caused any confusion about MFC's identity and objectives as I had summarised my sense of the discussion at the end, saying among other points, that MFC seems to play three complementary roles: a thought current and platform for exchange of ideas related to people's health,  an organisation coming together for collective action at times of a crisis, and as a circle of friends. 

Anant gave lots of context:

> The point, that MFC is a thought current and not an action-platform was made and pursued initially, I think by Ravi Narayan. All of us agreed. This is because our experience and our discussions made us realise that to become an 'action group' at national level required agreement on specific analysis on concrete  situations, issues also, like current policies on health care; some commonly agreed action plan is needed and some organizational inputs/strength is a must. But MFC was and is quite deficient in all these things. There was some broad agreement among us about our basic criticism of Health Care in India (partly summarised in the MFC brochure) and this was reflected in our discussions during the MFC Annual meets and the background notes for the Annual Meets, in the Bulletin, and these thoughts with the diversities in it, did constitute a broad pro-people thought current at the National Level. 
>
> The initial founder group consisted predominantly of some of the 'Tarun Shanti Sena' activists. They were broad enough in their overall outlook and  understanding to welcomed health-workers/doctors who came from other backgrounds like leftist background or humanist scientists like Kamala Jaya Rao, Mahatab Bamji as well as Community Health humanist teachers like Ravi, Thelma Narayan. (RSS types kept away or went away; I don't remember much). Thus there has been 'unity in diversity' in MFC, something which is quite good for a 'thought current' but not enough for an action-team. 
>
> Many of us were some kind of activists in our own areas but I don't think visiting and discussing community health projects was the only thing that attracted activists type young people to MFC discussions. Stimulating, passionate discussions about topics like irrelevance of medical education, about medical exploitation, role community health workers etc were also attractive features for newcomers. Going beyond ideologies, what attracted people like me (I was still in the twenties) was an atmosphere of  basic commitment of the MFC meet participants to the well being of the deprived people, the drive towards honesty, sincerity, humanism, austerity and friendly mileu; even though there were also some failings and unpleasant things, heart burns. Experts like Kamala Jaya Rao, Imrana were quite approachable. Any youngster would sit beside any such expert, on the floor in a round circle during discussions or during lunch/dinner break or during post-dinner sharing/singing session at night and ask any question or express oneself. There are no lectures, no presentations or no paper reading. At the same time, a lot of home-work done is done in the mid-annual meet and later, to draw appropriate questions to be discussed during the Annual Meet and to prepare background notes. Discussions have been rich in content.  In the last some years,  there have been presentations (but no paper reading) but MFC is perhaps the only National Forum in which half the time is reserved for discussions, which take place among participants sitting in a circle on the floor. 
>
> For many of us, post-dinner sessions have also been very valuable, likeable; these consist of 'sharing sessions' in which participants from varying backgrounds share their experiences, work, dilemmas etc; followed by singing sessions.
>
> I think all these things together keep attracting medicos/health-workers to this non-funded, organizationally amateurist outfit called MFC and it's Annual Meets, for the last almost 50 years. 
> 
> I have been saying that MFC is an open school in Community Health and for People's Health Movement with zero funding; no fees, no scholarships. It has made a great contribution in initiating, fostering, stimulating a critical, pro-people understanding in many of us. However, it's only a school which introduces people to the movement for Community Health and Healthcare, to a range of ideas, experiences in this field. (Further development depends upon our further explorations). Even if it's just a school I keep coming year after year to MFC-meets to learn and teach.  Sometimes, discussions in some of the sessions are somewhat frustrating; but discussion is not the only thing for which one goes to the MFC meet! Compared to many national, international meetings one has attended, I find MFC meetings to be more educational and satisfying.
> 
> Even as a thought current, this amateurist outfit (for which convenors, editors, e group managers have devoted a huge amount of time and efforts; Manisha has been managing the Registered Office for about 35 years !) has very little influence on the national scene even in the realm of debates, discussions. Nevertheless MFC is certainly a small candle which has helped many many souls to discover a meaningful journey in the health field and has encouraged them to play a small role in the big battle between the good and the evil in this field! Perhaps the younger generation can take things to a higher level!


Rajan had written in a 2008 email

> That, JSA is an action group and MFC is only though current etc are again individual opinion only. These are by no means organizational rules. MFC in fact is just not  thought current but has been action platform  e.g. in 1984 Bhopal gas study looking into reproductive outcome of exposed population or mfc¢s response to Gujrat communal violence etc MFC has been very much been action oriented as well. Even the no. of signature campaigns as  mfc takes as part of its advocacy is evidence that it is very much action platform. All the activies that is happening in MFC to get binayak sen released is vey much action by MFC. So please don¢t get confused- to get the exact  information please visit the websites www.mfcindia.org  for MFC and www.phmovement.org for PHA

### Harms

#### To mean it is not for action

This single word conceptualization has often been used as an antonym of "action" and has proactively or reactively stopped action. Examples:

In an email about why engage with mfc, [Srivatsan wrote](https://groups.google.com/g/mfccircle/c/VfB2l5lla1A/m/2qqVu558AwAJ):

> "In spite of the fact that it is a 'thought current' (Ah! But how powerful can a thought current be?), it is not 'activist' doing anything on the ground.  it is addressing this fundamental question of perspective -- sometimes crudely, sometimes clumsily, often ineffectively, but never stopping."

In a thread about passing away of members, Anurag suggested interviews be made available, to which [Sunil replied](https://groups.google.com/g/mfccircle/c/4OVeAzL9WWs/m/DUtJtEDWAQAJ):

> "I thought we were a thought current only ! "

In a thread about the mfc meeting on caste, Anant [had to explicitly remind people that mfc can do action](https://groups.google.com/g/mfccircle/c/al8DPXYviiA/m/OMjyou7oAwAJ)

> Even if MFC is predominantly seen as a  'thought current' a sub-group in MFC can come together for focussed discussion and collective action. In MFC such "cells" were formed and did make some progress, even though all of them withered away in due course - Primary Health Cell, Rational Drug Policy Cell, Women and Health Cell -----. It seems that many new, younger people are coming to this meet. If some of them want to form a "Cell" to primarily do some practical work collectively on a particular issue, under MFC banner, in my view, it is compatible with MFC as a thought current. The perspective and ethos of the "Cell" should be within the broad perspective and ethos of MFC. Details can be worked out. MFC as a whole maybe a thought current, (experience has made us realise this) but I don't think that it means that collective activity is discouraged; it is to be welcomed.

That this has been the case for a long time is seen in Amar's 2001 email to Saras:

> I was in two minds about using eForum to reply you on this, but then decided to use it thinking that pewrhaps others can also give their opinion. In last many many years we have had few rounds of intense discussion and also efforts to build local MFC groups (the Bombay Group that worked actively from 1988 to 1993 was one such very instense effort) to take up activities on regular manner. However, the MFC is regarded by most of members as only thought current, and it takes up activities as organisation only occasionally. And of course every five years some members also feel we are not able to do justice even as thought current, and so we discover crisis, debate it and revitalise the organisation again. Normally the fuel produced from such revitalisation effort lasts for another five years.
> 
> I don't know whether such a cycle (I won't call it vicious cycle as it is very addictive one) should continue, but it does. And no it is not funny, but there is lots of fun in it.


#### To mean "anything goes"

In a conversation where Nalini was unsure about whether to post something to the e-Group, [Srivatsan said](https://groups.google.com/g/mfccircle/c/wUYIX1H90Q8/m/izSbAboYCwAJ):

> "Nalini had some doubts about whether MFC would be offended by these posts, so I took the call since we are a thought current..."

#### To escape accountability and comprehensiveness associated with professionalism

Rakhal [in an email about caste and mfc](https://groups.google.com/g/mfccircle/c/0-H-5lRe3Oo/m/oEa8z4QABQAJ) said this:

> "In other words we as a group and thought current have resolutely refused to engage with the issue of caste."

Amar wrote in 2006 about how this is tiring:

> Frankly, I do not know what is expected. The MFC takes pride in being an amateur group, refusing to either institutionalise or professionalise. It even refuses to take support of other institutions or NGOs. It also refuses to have its permanent office, does not want organisational structure, does not want to dilute its position by having collaboration with other organisations in holding its annual meet - so on and so forth. The MFC does not believe in organisational activism - it wants to stay only as a "thought current" and work not with organisational professionalism but with the goodwill of "friends" - a few individuals who have mission to be 'shahid' for the thought current, and once 'kurbani' of that person is taken (he she gets burnt out), we wait for another 'shahid' to appear. But indeed, we will never agree to make MFC an organisation. We all have tried to change it from time to time, and miserably failed. When I try to change it, somebody else shows indifference to that effort - I get tired and so retire. You try it, I show indifference. I do not know when you will retire from this.
> 
> We have consciously chosen this way to work. So let us enjoy meetings withfriends once or twice in a year rather than getting worried about why theMFC is organisationally not so vibrant.

### Alternatives

Sathyamala proposed 'epistemic community' as an alternative:

> Regarding the role of mfc, there has been some discussion on the use of the term ‘thought current’, unfortunately, at times in a ridiculing manner. Well, I do believe the term thought current is quite apt as the ‘thinking’ within the mfc has electrified action elsewhere just as thinking elsewhere has electrified mfc – for instance, the annual meeting on the sexist bias in medicine in Anand. If ‘thought current’ as a term is not good enough, how about calling mfc an ‘epistemic community’...
