---
title: "What is mfc?"
---

This is a couple of emails that I wrote to mfc e-group under the title "What is mfc?"

### 26 Feb, 2023 ###

Priyadarsh, Savitri, and Nidhin, (or whoever is the decision making authority in mfc),

This email is rather long. It is therefore divided into these sections:

Introduction - just laying the context of why this email

Patiala Discussion - literature review

Analysis - my analysis (however flawed it maybe)

**Introduction**

In today's discussion on mfc's response to Bhopal gas tragedy, there was the question of why mfc didn't continue its engagement with Bhopal.

And it was met with the answer that mfc is not capable of sustained engagement with issues like that. A couple of people also suggested that people have agency to work on whatever they think is the priority and it is unfair to ask people to work on certain topics.

It then begs the question what is mfc? (Actually, "WTF is mfc?", but I suppose we can avoid that question).

A popular answer that is repeated ad-nauseum is that mfc is a thought-current.

What is a thought-current?

I tried google search and the first result is from <https://nithyanandapedia.org/wiki/Thought_Current> - an encyclopaedia set up by Arunachalam Rajasekaran popularly now known as Nithyananda - the Godman who set up the "country" called Kailaasa and is being chased by Gujarat police, with multiple rape allegations on him.

Many of the results that follow are also from videos of the same person.

Since I refuse to engage with a potential rapist on a topic like this, I am not going to use any of the definitions I find from there.

**Patiala Discussion**

On searching further I found a result from mfcindia's website itself, in the document titled "Anecdotes from a journey of thirty five years" <http://web.archive.org/web/20170319005727/http://mfcindia.org/mfcpdfs/history2.pdf>

It mentions "thought current" 5 times without defining it anywhere.

The first mention is in this sentence from Ravi's  article: " In the in passive phase which started around 1997, I had got quite disillusioned by the 'thought current preoccupation' of mfc and its addiction to the 'discussion mode' with no concerted effort to shift into action mode -- Bhopal and AIDAN not with standing".

The second mention is in the same article, "The bulletins reflect the shift our team tried to infuse into the 'thought current' by focusing on action, interactive dialogue, news from the field, keeping track of publications, placements available, etc to make mfc more 'alive and networking and engaging and acting'. The Bhopal involvement, the birth of AIDAN, the TB dialogue, Medical Education engagement and Environmental health dialogue were all actions that got a boost during this phase."

The third mention in the same article precedes a table called "Potential roles for mfc (The Patial Discussion - 1985)

[QUOTE]

The table taken from the background paper of the Patiala discussions gives\
an overview of the wide range of roles, that a thought current was beginning to think about. ( see also appendix -1 )

POTENTIAL ROLES FOR MFC (The Patiala Discussion -- 1985)\
a) Evolving /evaluating alternative health care strategist at field level.\
b) Critical evaluation and analysis of national health programmes and health care approaches.\
c) Acting as a forum for raising health issues and organizing campaigns.\
d) Monitoring health policies and playing a watch dog roles.\
e) Influencing health policy by lobbying and legal action.\
f) Medical activism which would include organizing people around health issues.\
g) Investigative research with a critical social perspective.\
h) Documentation, collection, review and dissemination\
i) Participating /linking with other groups in a health action network.\
j) Consultancy/support work for community health projects.\
k) Organising field orientation for medicos and others to sensitize them to broader social issues in health.\
l) Building stronger links with members through sharing of experience and evolving common perspectives

[END-QUOTE]

The fourth mention is in this (interesting) section on "The Bhopal disaster and its aftermath" right after this which starts with "The Bhopal disaster was in some ways, a major existential experience for mfc because it precipitated a strong action response in a thought current that had resisted such active responses in the earlier phase."

The final mention of thought current is in the concluding section of this article which goes something like:

[QUOTE]

9\. Reviving and expanding the circle: A continuing challenge\
mfc has always had a dialectic tension about its initial formulation as a thought current. While all of us have appreciated the need for serious reflection, listening to many points of view and evolving a comprehensive critique of a health problem or health challenge of our times, there has been a continuous sense of dissatisfaction about the resistance to action which often leads to debate, dialogue, some times even dissension. This has been going on for a long time and I particularly remember the build up to the Patiala discussions in 1985, which was provoked by papers by Anant and Ashvin and responses by Abhay and others on questions such as - Why are we discussing role?; What are the questions and issues before us?; What are the roles we would like to play?; and what are the alternative strategies and experiments?; and what could be the role of the bulletin?.

[END-QUOTE]

In the same article, there's a box which quotes from Anant Phadke's letter:

[QUOTE]

WHAT IS MFC?\
Mfc is a progressive broad front between socially conscious medicos of all kinds- from Gandhians to Marxists. We come together with a certain minimum understanding and stick to the common perspectives during our discussions. Each one has to keep his/her own ideology/jargon a bit more to one self and operate, discuss within the parameters of the common framework of medico friend circle. It is through this tradition of restricting oneself to the common perspectives and language during discussion that mfc has been able to hold together politically diverse elements on a common platform. Admittedly this does reduce the sharpness of analysis to a certain extent. But bringing together medicos fundamentally critical about the medical system of India is today a very important task that some organization must take up. Mfc has evolved as such a kind of\
organization.\
FIRSTLY, we feel that persons from different (but basically pro-people) background scan together for a meaningful discussion if all of us observe certain norms. If every body completely sticks to his/her framework and political language then different people would talk in different language and a meaningful discussion would become almost impossible.\
SECONDLY though almost all members of mfc are socio-politically oriented, we have kept directly political issues out of our discussion and confine it to the politics of medical care only. As we go nearer to the directly political question, differences, emerge sharply and a common consensus cannot emerge. It would therefore be better to refer to general political issues only if they are directly related to the point being discussed. Within a broad pro-people consensus, there are bound to be difference of opinion and they are indirectly linked to politics. But if different groups/individuals coming together start 'exposing' each other politics, then the purpose of mfc meeting would be defeated; there would be political polemics and not a discussion on the topic. AS an organizersof such discussion, we are concerned to see that these discussions are fruitful and the above lines may please be read in that context.\
From a letter written to all socially conscious medico in West Bengal -- Anant Phadke, October, 1983

[END-QUOTE]

In essence, I don't see thought current defined anywhere. But what I get a sense is that the definition of thought current is something like a forum for discussion, learning, and perhaps even some amount of brainstorming. (Because it is constantly used in contrast with any sort of action). And that's in line with what Anant Phadke has said today too.

The Appendix 1 and 2 of this document gives some context on the history of this question, as to what mfc is.

Appendix 1 is the kind of requests mfc was getting for collaboration/work.

Appendix 2 is the minutes of the Patiala discussion.

(What we will realize on reading this is that there are very few people working in this field and that a lot is requested from them. The expectations are high (like Thelma said in the meeting today). I'll elaborate my understanding of this later)

In Appendix 2 there's a reference to the 116-117 issue of bulletin where this is supposedly summarized. <http://web.archive.org/web/20220816225547/https://mfcindia.org/mfcpdfs/MFC116-117.pdf>

That article, "Reporting from Patiala", has some interesting sentences.

"The circle has been basically a forum for discussion, dialogue and experience sharing of individuals involved in socially relevant heath action

"One of the ways we could go about it is to have small cells---groups of members\
who review creatively roles and needs around well defined areas, eg: critical analysis and monitoring of health policies and programmes; alternative approaches in community health care; communications and lobbying for health action; investigative research---priorities, issues and relevant methodologies; This group work may help to enrich the circle and move it along the new dimensions of the future. From amateurishness to scientific rigour; from personal involvement to collective action; from adhocism to planned development"

"The discussion about role of mfc at Patiala were a beginning of a process of reflection and debate which must go on till the 'light is seen'."

What I therefore understand is that there was no conclusion in the Patiala meeting. (People who were there can please correct me if I'm wrong).

[ASIDE]

This article also includes some snippets on Bhopal:

Pregnancy outcome study\
The study on the effect of toxic gases on pregnant women, scheduled for July was\
postponed to September (22-29). The objectives of the study will be to assess the\
increase in spontaneous abortions, still birth rates and congenital malformations in the affected population. They study is being undertaken in coordination with many other organisations with Sathyamala of mfc as coordinator. (For further information, contact Sathyamala, C-152, MIG Flats; Saket, New Delhi 110017).

Future role

While reiterating its technical, research and communication support to the healthgroups working in Bhopal, both voluntary and governmental, a local review was thought necessary. Anant Phadke will base himself in Bhopal for a few weeks in August-September to identify the future and continued role. It is hoped that a broad network of voluntary agencies and health action groups will coalesce so that meaningful health efforts can be continued. A health communication effort with the basti dwellers will also be explored.

[END-ASIDE]

**Analysis**

What I understand is the following.

The challenge seems to be that there is lots to do and not enough people to do everything. The idea of thought current helps to avoid the mess of prioritization.

If that's the case, mfc doing an activity *as mfc* in Bhopal was a mistake. Because mfc couldn't have done justice to an intervention like that. But mfc did do it. This could be considered as a form of volunteer tourism, charity tourism, etc. Wherein volunteers who have *some* time to spare visit a place and do something. A nice article about this can be read at <https://medium.com/@vijaykumar.malavika/what-is-volunteer-tourism-and-when-can-it-actually-be-bad-fa111c329191> wherein the author Malavika Kumar writes about the dangers of volunteer tourism and what can be done to avoid it.

But only individuals can do volunteer tourism. A Yogesh Jain can go as a medical student volunteer. When an "organization" goes to do volunteer tourism - the way mfc has done - it doesn't really fit the definition of volunteer tourism. It feels more like a short term collaboration between two or more organizations. A short term alliance with resource exchange.

Perhaps then what happened during 1984 might be that a poorly organized organization like mfc, as a group, due to the passion for people, decided to engage *as the organization*, without thinking about whether a sustained engagement would be necessary or whether mfc as an organization is capable of sustained engagement. By the standards of today's NGOs, these short term alliances are quite natural. mfc did what mfc needed to do in 1984, whether it would have been sustainable, whether it needed sustainability, all of that didn't matter much.

And that's how a lot of human beings work. Not all of us like to be stuck in one thing forever. We like to do new things, work on new challenges, grow and experience. Then, there's the question of ethical responsibilities of social action. Should those who engage in social action necessarily create a sustainable mechanism of such action? Should all social action be contingent on the ability to be sustainable?

If we let go of perfectionism and embrace uncertainties, then it is clear that mfc doing what mfc did in Bhopal as mfc is justifiable. So let me leave Bhopal and come to caste.

The next annual meet is going to be around caste. Does that mean mfc is going to have any commitment towards caste? From the previous analysis, it is amply clear that it does not mean so. Then what is the point of hundreds of people coming together to meet, travelling hundreds/thousands of kilometers, burning fossil fuel on the way?

I hear at least two answers as to why mfc meeting is still useful:

1) It helps some people learn.

2) It helps some people connect with similar minded people.

Essentially, it becomes a forum for "discussion, dialogue, and experience sharing". (I completely discard the idea that mfc inspires people to start organizations for the lack of evidence proving the causal relationship between the two).

So, that's what mfc is. A forum for discussion, dialogue, and experience sharing. I would go one step ahead and call it "a tightly guarded network for exchange of social capital".

It is tightly guarded because participation can only be through these annual meetings or through eGroups which are not public.

It is a network for exchange of social capital because that's what helping each other learn and "connecting" means.

Now I don't have a problem with that except for the fact that being tightly guarded essentially makes it an exclusionary place. To eliminate that and make it "an open network for exchange of social capital", I would suggest the following:

1) Make mfc eGroup publicly accessible and web searchable.

2) Make mfc eGroup moderated, adopt a code of conduct that explicitly prohibits discrimination and makes it unacceptable to connect people by their caste name, for example.

3) Let mfc have a presence on social media - especially twitter and instagram where lots of discussion and dialogue happen these days.

4) Have the decision making structure of mfc be correctly documented and the procedures related to how mfc makes decisions be transparent. This should ideally be democratic as well.

5) ...

6) ...

7) ...

I can discuss these with you if you think these are sensible. I am willing to work on this, provided that 1) there is a decision making structure in mfc, 2) that structure is willing to commit to the goal of opening up the network and making it accessible to others.

Let me know.

ASD

### 3 March, 2023 ###

Thank you everyone who responded (on and off list). I'm putting together some more conceptualizations and some more analysis:

Ritu in a previous thread had written this:

> MFC seems to play three complementary roles: a thought current and platform for exchange of ideas related to people's health,  an organisation coming together for collective action at times of a crisis, and as a circle of friends.

~ <https://groups.google.com/g/mfccircle/c/nzuI_fLJeS0/m/lDyBtIvdCAAJ>

Mira is also saying "circle of friends".

Ravi Narayan wrote this in private email:

> All mfc members therefore  had two hats ! The mfc membership was always as an  Individual. But all had  another hat -an organizational back up.of one sort or another and could always draw upon that organisation to support a mfc led action to different extents.

Ashok Bhargava and I had a (private by mistake) thread which I'm copying here:

Ashok:

> Have you read the first brochure of MFC and future programs I shared on 26 February?

Akshay:

> Had seen when you shared it. Went through it now.
>
> What I see is a nice and practical list of goals that is commensurate with the name "medico friend circle". But then how does Bhopal fit into mfc's aims? Can we retract Bhopal and say it was a mistake for mfc to go into Bhopal **as mfc**?
>
> Also, where does the description of thought-current come from? Why is nobody opposing that description?
>
> And lastly, why should this discussion between you and me be private?

Ashok:

> It is not a private discussion. I might've pressed a wrong key.

> Bhopal was a case of fire fighting at the most, in which a small group got involved. MFC is a group of individuals who come from different backgrounds, ideologies, interests and priorities. Some of them come to annual meet regularly, others come when the topic of the meet is interesting for them. MFC is not an NGO, institution, union or political party. Persons associated with MFC take part in different activities and movements in their individual capacity. Many groups organized medical camps and other activities at local and state level in the initial phase. Some of us are running health projects, training programs, advocacy groups, etc in which we regularly interact with other MFC members.\
> MFC is a unique democratic group in which we discuss health problems from different points of view in wider perspective to bring about change in medical system and society at large. I don't know who introduced the "thought current".\
> Please share it with the thread.

I think, then, like Ritu mentioned:

- mfc's response to Bhopal and AIDAN may be just collective action in response to crisis.

- mfc is primarily a circle of friends that serve to exchange ideas and thoughts.

Which means conceptually, my thinking on mfc now shifts from wanting mfc to be a regular place for collective action (mfc is not organized in a way that allows regular collective action), to a conceptualization where mfc is a safe space for those who want to escape the biomedical and find social validation through validating discussions.

Like Anant said in the previous thread:

> a small candle which has helped many many souls to discover a meaningful journey in the health field and has encouraged them to play a small role in the big battle between the good and the evil in this field

Somehow I think the "Philosophy" section in the old website had led me to the misimpressions about what mfc is. I think it's clearing up.

The new website says this about mfc:

> It is an organization which has operated for forty eight years as a 'thought current' without allegiance to a specific ideology. Its only commitment has been to intervene in and understand the debates, policies and practices of health in Independent India. The understanding that our present health service is lopsided and is in the interest of a privileged few prevails as a common conviction.  It has critically been analyzing the existing health care system and has tried to evolve an appropriate approach towards health care which is humane, and which can meet the needs of the vast majority of the people in our country.

I suppose the phrase "tried to" makes even the last part accurate. Kudos to whoever added this to the website. It goes a great way in giving new people who join an accurate representation of mfc.

It fails to capture the stories of the friends in the circle, though. Perhaps that's the only missing piece of this puzzle.

ASD