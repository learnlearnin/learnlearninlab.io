---
title: "Obstetrics"
description: awww.. babies!! and ewww.. leaky vaginas
keywords: ob, obstetrics, obg
---
Giving birth could be a deadly process.

[This WHO article](http://www.who.int/bulletin/volumes/85/10/07-045963/en/) says how post-partum care is very important and neglected (true!)

### Breast Crawl ###
This is a reflex that is rarely seen in the hospital because babies are separated from mother as soon as they are delivered. [Read Taru Jindal's blog on lactational issues arising from not allowing breast crawl](https://www.girlsglobe.org/2017/06/06/babies-born-breastfeed/). Go through [breastcrawl.org](http://breastcrawl.org/) which is a website dedicated for this.

More on breastfeeding at [World Alliance for Breastfeeding Action](http://waba.org.my/)
