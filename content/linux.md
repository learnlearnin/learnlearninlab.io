---
title: Linux
description: Linux is the kernel that powers GNU/Linux.
keywords: linux, os
---
Linux is the powerful kernel that runs Android, [GNU/Linux](../gnu-linux/) operating systems, and everything that we see around.

## Install ##
You probably don't want to install Linux if you don't want to:
* Learn about how computers work
* Learn about the boot process
* Learn about partitioning
* Learn about hardware drivers
* Learn about internet configuration
* Learn about tiny details of your computer which makes things work
* Learn how to figure out things by yourself
* Patiently search for answers through internet
* Figure out how to use search engines to find out answers
* Figure things out by yourselves

If you feel intimidated by the above list for just wanting to "try out Linux", you probably just want to use Linux. Using Linux is entirely different from "installing" Linux. If you just want to use Linux, use an Android phone. If you want to use something GNU/Linux, probably buy a laptop with Ubuntu pre-installed. Installing Linux, like installing any other operating system (including Windows) takes a lot of patience, learning, and understanding.

If you really want to install Linux on your computer, here's what you should know.

### Distros ###
There are a thousand *distros* or *distributions* of (GNU/)Linux. These are superficially similar variants with different software packages included in it. They are built by different people for different reasons. Not all distros are the same.

If you are a total beginner, you should probably install Ubuntu just because it has an awful lot of questions about it answered painstakingly by various people on the internet.

If you have already used Ubuntu or a different distro and you're reading this article, you already know how to choose a distribution and therefore, you don't need my help.

But, just in case, my favourite distribution is ArchLinux.

### Installation Guide ###
There will be an official installation guide available for most distros and certainly Ubuntu. Read the entire thing before you start destroying your computer. Make sure you understand what's coming ahead.

### Internet ###
Linux distributions thrive because of internet. All the documentation, forums, questions and answers, errors, mails, communications, are on the internet. If you run into an error, chances are that hundreds of people have run into the same error and at least tens of people have written about it on the internet. Learn how to read. Read error messages. Figure out how to use a search engine. Search those error messages. And use a lot of common sense.

For example, if the error message says "no disk inserted", think of what that means before you start searching for why there's an error.

### Partitioning ###
Yes, you will have to partition your disk. And no, I'll not tell you what that means. You are better of figuring it out on your own. Here's a hint: all your data is at stake. Backup.

### This thing does not work ###
No. It will not work. And no, nobody else can fix it for you and neither will it get magically fixed. You'll have to figure out why it is not working and find a fix yourself.

### Is it worth the bother ###
Yes.

### Can you help me? ###
Maybe. You can [ask me smart questions](http://www.catb.org/~esr/faqs/smart-questions.html). [Ping me on telegram](https://asd.learnlearn.in/about/#contact).
