---
title: Mental Health
description: Mental Health, mental illnesses, psychiatry, psychology
keywords: mental health
---

## Depression ##

[PHQ-2](http://www.cqaimh.org/pdf/tool_phq2.pdf) is an easy tool for screening depression. [PHQ-9](http://www.cqaimh.org/pdf/tool_phq9.pdf) is a complete tool for diagnosing, and monitoring as well.
