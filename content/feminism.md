---
title: "Feminism"
description: "Equality for all"
keywords: feminism, equality
---

**Update: This page was initially written when I used to think of gender as a binary + other. But now I think of gender as a fluid concept and this page hasn't fully been updated to include that**

I am an intersectional feminist.

That is, I believe that there are a lot of inequalities in our society (gender, caste, race, etc in no particular order), that the effect of these inequalities get compounded disproportionately in the intersections, and that a lot of work is needed to eliminate such systemic oppression.

In practice, there are multiple kinds of feminism. Let us go through some examples:
* [HeForShe](https://www.heforshe.org/en) - a movement where the focus is on gender alone and explicitly wants to avoid misandry by including cis-men in the equation.
* White feminism - which is focused on breaking glass cielings and often misses the perspectives of race, caste, etc.
* [Intersectional feminism](#intersectional-feminism) - which speaks about the additive effect of belonging to an oppressed gender, race, caste, etc.
* [The extremism where women label everything that a man does/says "sexist" and attack men whenever possible.](#extremist-feminism)
* TERF - trans-exclusionary radical feminist.

## Discrimination ##
Gender discrimination is so prevalent and systematized that we often fail to see where they exist. Here's a list of links which point things out:
* [An Open Letter to Kerala Khap Managers and Madam Principal, CET, in Particular, and to Malayalees in General](http://kafila.org/2015/03/28/an-open-letter-to-kerala-khap-managers-and-madam-principal-cet-in-particular-and-to-malayalees-in-general/): talks about schools and colleges putting restrictions on what a girl can do, and how she must behave within the bounds of their institution and sometimes even outside. Unfair curfews at hostels under the pretext of security, for example.

## Rape ##
It is a violent crime perpetuated by common people when they think the conditions are right.

## Organizations ##
These organizations are feminist.
* [HeForShe](http://heforshe.org/) - an international organization with Emma Watson leading it.
* [Blank Noise](http://blog.blanknoise.org/) is a movement which encourages action heroes to do things like they would want to do without having to listen to patriarchies. It is active in many cities of India.

## India's Daughter ##
[India's Daughter](../indias-daughter/) - A documentary by Leslee Udwin about December 16 gang rape and murder of Jyoti Singh in Delhi. It documents the attitude of the guilty, their guilty lawyers, victim's friends and relatives, the public, and the judiciary. Some horrifying defences by the lawyers of the guilty - like "A woman is a flower, she should be protected, a man is like thorn, strong and tough.", and so many more worse things. Read the controversies surrounding it [here](../indias-daughter/)

## Bollywood ##
I am absolutely sure that Bollywood plays the biggest role in perpetuating patriarchical culture and an image of women that they are sex objects. In fact, I did a small not so scientific study to see [what is focussed in Bollywood songs](../focus-in-movie-songs/). You can see the results for yourself.

### Deepika Padukone ###
Deepika had a cleavage row in which she blamed TOI for focussing on her cleavage. But [I call Deepika a hypocrite, read about it on quora](http://qr.ae/BLqSR)

But she also featured in this Vogue campaign called ["My Choice"](https://www.youtube.com/watch?v=KtPv7IEhWRA) to which an apt reply would be [this](http://theladiesfinger.com/what-vogue-said-what-we-heard/) and which probably belong to [extremist feminism](#extremist-feminism)

She apparently holds this opinion on feminism:
> "New feminism isn't about being aggressive; it's about reaching the top yet being soft. It's about being you — feminine, strong and full of will power." <footer>[Consumer India](http://books.google.co.in/books?id=Uw-XCvE9G4YC&q=new+feminism#v=snippet&q=new%20feminism&f=false)</footer>

## Intersectional Feminism ##

White feminism usually answers questions like "[Does reverse sexism exist?](https://asd.learnlearn.in/reverse-sexism/)" with "No". But in intersectional feminism, the answer is "it is complicated".

Also read my post [Is Feminism Brahmanism](https://blog.learnlearn.in/2020/06/is-feminism-brahmanism.html)

## Extremist Feminism ##
Extremist feminism is what some irrational females end up doing by blaming men for any action they do. They would even consider using the word "female" as a sexist remark.

Here are examples of how they create more problems than solve (if any)
* [A Dongle Joke That Spiraled Way Out Of Control](http://techcrunch.com/2013/03/21/a-dongle-joke-that-spiraled-way-out-of-control/)
* [In defence of Rosetta mission scientist Dr Matt Taylor and his controversial shirt](http://metro.co.uk/2014/11/15/in-defense-of-rosetta-mission-scientist-dr-matt-taylor-and-his-controversial-shirt-4949004/) - women can wear anything, men can't.


## India ##

### Manusmriti ###

* [How the courts misuse Manusmriti to deny women constitutional rights in India](https://mailchi.mp/thenewsminute/the-next-wave-2476778)

## Gayatri Spivak ##

* [Interview with Anshul Kumar](https://www.newindianexpress.com/cities/delhi/2024/May/30/interview-anshul-kumar-on-gayatri-spivak-and-the-circle-of-sycophancy)
