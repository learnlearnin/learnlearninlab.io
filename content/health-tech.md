---
title: Health Technology (Digital Health)
description: Digital health, digitalization, etc
keywords: health-tech, health technology, digital health
---

## Nuanced takes

* [Is digitalization a double edged sword?](http://ideasforindia.in/topics/productivity-innovation/is-digitalisation-a-double-edged-sword-for-workers-in-indias-public-healthcare-system.html)


## Outline of work pending in digital determinants of health

### Improving tech literacy

People are too keen to say "I'm not tech-savvy" and in the same breath take important decisions about public health that involves technology. The literacy rate among decision makers is too low. And the technologists capitalize on this. The following concepts need to be popularized and made common knowledge (for digital health).

* domain name
* server, client
* online, offline
* browser, device, app, website
* database, data
* API, internet
* source code, licensing
* architecture
* federated, centralized, decentralized
* account, identity, authentication, authorization
* UI, UX, design
* accessibility

Without these basics, people cannot make any sense of digital technologies.

### Problematizing tech exceptionalism

> “Tech exceptionalism” is the sin of thinking that the normal rules don’t apply to technology.
> 
> The idea that you can lose money on every transaction but make it up with scale (looking at you, Uber)? Pure tech exceptionalism. The idea that you can take an unjust system like racist policing practices and fix it with technology? Exceptionalism. The idea that tech itself can’t be racist because computers are just doing math, and math can’t be racist? Utter exceptionalism.
> 
> ~ [Cory Doctorow](https://www.wired.com/story/the-internet-con-cory-doctorow-book-excerpt/)

When people understand how technology works, there is a chance that they will start appreciating what problems it can solve and cannot solve. The lies by techno-capitalists will get easier to expose.

It is also possible to directly problematize tech exceptionalism. We can stress on how the problems of technology are not problems of the technology itself. Instead they are problems of the humans using those technologies. And we have centuries of experience dealing with how human beings can and should be regulated.

### Exposing industry interference

Much of what's now called "digital public infrastructure" can be traced back to what's called the "Koramangala Gang" led by Nandan Nilekani. The push for digital in every space quickly followed by data monetization is a playbook that is being applied repeatedly.

Much of this push is built over lies. Lies like:

* Lie: Millions of transactions is symbol of success
* Lie: Software like UPI is "open" or "federated"
* Lie: Platforms like ABDM are open and federated
* Lie: The domains like health are just extensions of domains like fintech
* Lie: Digital transformation is always good, and it is mandatory opt-in to things like ABDM that fuels digital transformation
* Lie: Innovation is made possible through such centralization
* Lie: Private capital has to be attracted through such central platforms for anything good to happen

Several such lies contribute to an overwhelmingly stupid idea of what's inclusive growth.

### Critical and advanced higher education modules

A huge lacunae lies in building educational modules that are cutting edge both in its radical social justice outlook and in its technical brilliance. For example, here are some sample education modules that should exist:

* designing accessible user interfaces (accessibility in terms of language, literacy, disability, etc)
* designing offline first software (including solving distributed unique identity, federated/decentralized architecture)
* constitutional morality and its relevance in engineering, medicine, and law
* interdisciplinary problem solving approach to public health technology problems


### Policy gyaan

It is impossible to have policy around technology in such a premature society. All policies will be subverted by either practical pressures or greed. Yet there might be value in providing policy gyaan to project a sense of comprehensiveness in work, and also because many people think policy making is the most important silver bullet.

There might be value in engaging with those who spend time in this space.

### Digital Humanities

There is great value in bringing lots of digital work together. For example:
- digital story telling
- critical digital pedagogy
- a directory of good software
- self-hosted services like archives, wiki


### Distractions

There are so many questions that are actually distractions.

* Consent
* Privacy
* Confidentiality
* "Bioethics"

But if there's value in exploring some of these for political reasons, they can be engaged with.

### Potential Collaborators

* IPH Bengaluru
* IT for Change - Anita and Guru
* CIS
* IIT Delhi Reetika Khera
* MKSS
* SAFAR
* SOCHARA
* ...
