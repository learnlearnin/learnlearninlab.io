---
title: Database
description: How to store data
keywords: cs, computer science, programming, coding, database
---

## Migrations
* [Gitlab guide](https://docs.gitlab.com/ee/development/migration_style_guide.html)
* [Common mistakes](https://postgres.ai/blog/20220525-common-db-schema-change-mistakes)
