---
title: Parenting
description: How to be a good parent
keywords: parenting
---

* Reward effort, not result. [[Source](https://twitter.com/_youhavemyword_/status/1194976086574026758?s=20)]

