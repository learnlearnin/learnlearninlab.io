---
title: Leadership
description: What is leadership?
keywords: leadership
---

## Resources ##
* [Learning Charisma](https://hbr.org/2012/06/learning-charisma-2)

## Fixing team
* [How to fix a team](https://scottberkun.com/2010/how-to-fix-a-team/) - practical suggestions on how to start and what to do
